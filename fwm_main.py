#!/usr/bin/env python3
import os

import pygame
import pytmx
from pygame.locals import *
from pygame.sprite import collide_rect

from Collectible import Collectible
from GamePlayer import Player
from GameTile import GameTile
from Dangers import Fire, Water
from Grid import Grid
from Ice import Ice


class FWMMain():
    def __init__(self):
        """ Init pygame """
        pygame.init()

        # Boolean values to know where we are in the game
        self.game_started = False
        self.game_paused = False
        self.game_ended = False

        # Clock
        self.clock = pygame.time.Clock()

        # Setting up the display
        pygame.display.set_caption('Frozen Water Maze')
        icon_path = os.path.dirname(__file__) + os.sep + "assets/ico.png"
        pygame.display.set_icon(pygame.image.load(icon_path))
        self.screen = pygame.display.set_mode((64 * 18, 64 * 12))

        self.font = pygame.font.Font(None, 36)

        # Load data
        tmxpath = os.path.dirname(__file__) + os.sep + "assets/map.tmx"
        tmxdata = pytmx.load_pygame(tmxpath)
        self.game_tiles = []
        self.game_fires = []
        self.game_waters = []
        self.game_collectibles = []
        self.game_grids = []
        self.game_ice = []
        for coord_x in range(18):
            for coord_y in range(12):
                img = tmxdata.get_tile_image(coord_x, coord_y, 0)
                if img is not None:
                    self.game_tiles.append(GameTile(img, coord_x, coord_y))
                fire = tmxdata.get_tile_image(coord_x, coord_y, 1)
                if fire is not None:
                    self.game_fires.append(Fire(coord_x, coord_y))
                water = tmxdata.get_tile_image(coord_x, coord_y, 2)
                if water is not None:
                    self.game_waters.append(Water(water, coord_x, coord_y))
                collectible = tmxdata.get_tile_image(coord_x, coord_y, 3)
                if collectible is not None:
                    self.game_collectibles.append(Collectible(coord_x, coord_y))
                grid = tmxdata.get_tile_image(coord_x, coord_y, 4)
                if grid is not None:
                    self.game_grids.append(Grid(grid, coord_x, coord_y))
                ice = tmxdata.get_tile_image(coord_x, coord_y, 5)
                if ice is not None:
                    self.game_ice.append(Ice(ice, coord_x, coord_y))

        # Init music
        music_path = os.path.dirname(__file__) + os.sep + "assets/sfx/bg_music.ogg"
        pygame.mixer.music.load(music_path)
        pygame.mixer.music.set_volume(0.25)
        pygame.mixer.music.play(-1)

        # Init ambient motor sound
        ambient_motor_path = os.path.dirname(__file__) + os.sep + "assets/sfx/motor.ogg"
        ambient_motor = pygame.mixer.Sound(ambient_motor_path)
        ambient_motor.set_volume(0.15)
        ambient_motor.play(loops=-1)

        # Init ambient droplet fall sound
        droplet_path = os.path.dirname(__file__) + os.sep + "assets/sfx/droplet.ogg"
        self.ambient_droplet = pygame.mixer.Sound(droplet_path)
        self.ambient_droplet.set_volume(0.3)

        # Init get sound
        get_path = os.path.dirname(__file__) + os.sep + "assets/sfx/get.ogg"
        self.get_sfx = pygame.mixer.Sound(get_path)
        self.get_sfx.set_volume(0.2)

        # Init game background
        bg_path = os.path.dirname(__file__) + os.sep + "assets/bg.jpg"
        background = pygame.image.load(bg_path)

        # Init player
        self.player = Player()

        # Init score
        self.score = 0
        self.score_image = self.font.render(str(self.score), True, (255, 255, 255))
        self.score_rect = self.score_image.get_rect()
        self.score_rect.top = 5
        self.score_rect.left = 64

        # Init timer
        self.timer = 20
        timer_path = os.path.dirname(__file__) + os.sep + "assets/timer.png"
        self.timer_image = pygame.image.load(timer_path)
        self.timer_rect = self.timer_image.get_rect()
        self.timer_rect.top = 5
        self.timer_rect.left = self.screen.get_width() - 64
        self.timer_text_image = self.font.render(str(self.timer), True, (255, 255, 255))
        self.timer_text_rect = self.timer_text_image.get_rect()
        self.timer_text_rect.top = 24
        self.timer_text_rect.left = self.screen.get_width() - 92

        # Startup screen display
        startup_img_path = os.path.dirname(__file__) + os.sep + "assets/title.jpg"
        self.startup_image = pygame.image.load(startup_img_path)
        self.screen.fill((0, 0, 0))
        startup_screen_display = True

        while startup_screen_display:
            self.screen.blit(self.startup_image, Rect(0, 0, self.screen.get_width(), self.screen.get_height()))
            pygame.display.flip()
            pygame.time.wait(3000)
            startup_screen_display = False

        # Init internal event -> droplet fall
        pygame.time.set_timer(pygame.USEREVENT, 5000)

        # Init internal event -> timer decrease
        pygame.time.set_timer(pygame.USEREVENT + 1, 1000)

        # pygame.USEREVENT + 2 is used to shapeshift into cloud
        # pygame.USEREVENT + 3 is used to avoid constant shapeshifting call when in cloud mode
        # pygame.USEREVENT + 4 is used to slide in ice mode

        # Game loop
        while not self.game_ended:
            self.screen.fill((0, 0, 0))
            self.check_game_event()

            self.screen.blit(background, Rect(0, 0, 64 * 18, 64 * 12))

            self.screen.blit(self.score_image, self.score_rect)
            self.screen.blit(self.timer_image, self.timer_rect)
            self.screen.blit(self.timer_text_image, self.timer_text_rect)

            self.player.is_falling = True

            for tile in self.game_tiles:
                tile.display(self.screen)
                if self.player.rect.bottom == tile.rect.top and self.player.rect.left == tile.rect.left:
                    self.player.is_on_ground = True
                    self.player.is_on_ice = False
                    self.player.is_falling = False

            for tile in self.game_grids:
                tile.display(self.screen)
                if self.player.rect.bottom == tile.rect.top and self.player.rect.left == tile.rect.left \
                        and self.player.is_in_icemode:
                    self.player.is_on_ice = False
                    self.player.is_falling = False

            for tile in self.game_ice:
                tile.display(self.screen)
                if self.player.rect.bottom == tile.rect.top and self.player.rect.left == tile.rect.left:
                    self.player.is_on_ground = False
                    self.player.is_on_ice = True
                    self.player.is_falling = False

            for danger in self.game_waters:
                danger.display(self.screen)
                if self.player.rect.bottom == danger.rect.top and self.player.rect.left == danger.rect.left:
                    if self.player.current_shape != Player.PLAYER_CLOUD:
                        self.game_ended = True
                        self.player.is_falling = False
                elif collide_rect(self.player, danger):
                    self.game_ended = True
                    self.player.is_falling = False

            self.player.fall()

            for danger in self.game_fires:
                danger.display(self.screen)
                if collide_rect(self.player, danger):
                    self.game_ended = True

            for point in self.game_collectibles:
                point.display(self.screen)
                if collide_rect(self.player, point):
                    self.score += 1
                    self.get_sfx.play()
                    self.game_collectibles.remove(point)
                    self.score_image = self.font.render(str(self.score), True, (255, 255, 255))

            if not self.player.is_on_ice and self.player.is_sliding:
                print("stop slide")
                self.player.is_sliding = False

            self.player.display(self.screen)

            pygame.time.wait(40)
            pygame.display.flip()

        pygame.quit()

    def check_game_event(self):
        """ Check game events """
        for event in pygame.event.get():
            if event.type == pygame.USEREVENT:
                self.ambient_droplet.play()
            elif event.type == pygame.USEREVENT + 1:
                self.timer -= 1
                if self.timer == 0:
                    self.game_ended = True
                self.timer_text_image = self.font.render(str(self.timer), True, (255, 255, 255))
            elif event.type == pygame.USEREVENT + 2:
                self.player.shapeshift(Player.PLAYER_DROPLET)
                pygame.time.set_timer(pygame.USEREVENT + 2, 0)
            elif event.type == pygame.USEREVENT + 3:
                self.player.can_cloudify = True
                pygame.time.set_timer(pygame.USEREVENT + 3, 0)
            elif event.type == pygame.USEREVENT + 4:
                if self.player.is_going_left:
                    self.player.move_left()
                else:
                    self.player.move_right()
                if not self.player.is_sliding:
                    print("stop")
                    self.player.can_move = True
                    pygame.time.set_timer(pygame.USEREVENT + 4, 0)
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_i:
                    self.player.shapeshift(Player.PLAYER_ICE)
                elif event.key == pygame.K_c:
                    if not self.player.is_in_cloudmode and self.player.can_cloudify:
                        self.player.shapeshift(Player.PLAYER_CLOUD)
                        self.player.can_cloudify = False
                        pygame.time.set_timer(pygame.USEREVENT + 2, 2000)
                        pygame.time.set_timer(pygame.USEREVENT + 3, 200)
                elif event.key == pygame.K_d:
                    self.player.shapeshift(Player.PLAYER_DROPLET)
                elif event.key == pygame.K_RIGHT:
                    if self.player.can_move:
                        self.player.move_right()
                        if self.player.is_on_ice and self.player.is_in_icemode:
                            self.player.is_sliding = True
                            self.player.can_move = False
                            pygame.time.set_timer(pygame.USEREVENT + 4, 50)
                elif event.key == pygame.K_LEFT:
                    if self.player.can_move:
                        self.player.move_left()
                        if self.player.is_on_ice and self.player.is_in_icemode:
                            self.player.is_sliding = True
                            self.player.can_move = False
                            pygame.time.set_timer(pygame.USEREVENT + 4, 50)
                elif event.key == pygame.K_UP:
                    is_move_possible = True
                    for tile in self.game_tiles:
                        if self.player.rect.top == tile.rect.bottom and self.player.rect.left == tile.rect.left:
                            is_move_possible = False
                    if is_move_possible:
                        self.player.move_up()
                elif event.key == pygame.K_DOWN:
                    is_move_possible = True
                    for tile in self.game_tiles:
                        if self.player.rect.bottom == tile.rect.top and self.player.rect.left == tile.rect.left:
                            is_move_possible = False
                    if is_move_possible:
                        self.player.move_down()
                elif event.key == pygame.K_f:
                    pygame.display.toggle_fullscreen()
                elif event.key == pygame.K_ESCAPE:
                    self.game_ended = True
            elif event.type == pygame.QUIT:
                self.game_ended = True


if __name__ == "__main__":
    FWMMain()
